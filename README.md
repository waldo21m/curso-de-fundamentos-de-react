# Curso para desarrollar servicios web REST con Nodejs

## Lecciones:

* [Lección #1 - Introducción](#lesson01)
* [Lección #2 - Editor en internet](#lesson02)
* [Lección #3 - Mi primer componete](#lesson03)
* [Lección #4 - Props](#lesson04)
* [Lección #5 - JSX](#lesson05)
* [Lección #6 - State](#lesson06)
* [Lección #7 - Trabajando con formularios](#lesson07)
* [Lección #8 - Colecciones](#lesson08)
* [Lección #9 - Peticiones AJAX](#lesson09)
* [Lección #10 - Estilos](#lesson10)
* [Lección #11 - React Hooks](#lesson11)
* [Lección #12 - Separar componentes en archivos](#lesson12)

## <a name="lesson01"></a> Lección #1 - Introducción
- - -
Introducción y explicación de React.

## <a name="lesson02"></a> Lección #2 - Editor en internet
- - -
https://stackblitz.com/ es una plataforma que nos permite tener un editor de código en línea donde podemos iniciar con cualquier clase de proyecto como: Angular, React, Svelte, entre otros.

## <a name="lesson03"></a> Lección #3 - Mi primer componente
- - -
Creación de un componente funcional y un componente de clase en App.js

*Todos los componentes deben estar escritos en UpperCamelCase, de lo contrario no funcionará*.

## <a name="lesson04"></a> Lección #4 - Props
- - -
**En un sistema siempre debemos procurar de evitar desarrollar elementos que dependan uno de otros**. Es decir, debemos desarrollar elementos independientes entre si.

Cuando desarrollemos componentes debe ocurrir lo mismo, pero ¿Qué ocurre cuando debemos compartir datos entre ellos? Para ello debemos usar las **props**. Las props son propiedades de creación del componente, es decir, información que establecemos para un componente cuando lo creamos.

Esta es la forma mediante el cual un componente padre pasa información a los componentes hijos.

Para esta lección, crearemos dos componentes A y B que reciban el props *nombre*.

**Importante: Las props son ReadOnly, no se pueden editar... Solo se pueden leer**

## <a name="lesson05"></a> Lección #5 - JSX
- - -
JSX es una extensión de JS creada por Facebook que nos permite usar código HTML y hojas de estilo CSS en archivos de JS.

Cuando creamos componentes, podemos pasarle hijos a dicho componente... Para esta lección pasaremos un hijo y lo llamaremos mediante *props.children*.

## <a name="lesson06"></a> Lección #6 - State
- - -
El estado es todo lo que puede sufrir una modificación dentro de un componente. En esta lección, manejaremos los estados en las clases.

Si un state sufre cambios, se verá reflejado inmediatamente en la aplicación. React tiene todo debidamente configurado para que emplee el virtual DOM y use la menor cantidad de recursos posibles. Esto se le conoce como programación reactiva.

## <a name="lesson07"></a> Lección #7 - Trabajando con formularios
- - -
Creación y manejo de un formulario con React.

## <a name="lesson08"></a> Lección #8 - Colecciones
- - -
En esta lección, manejaremos un arreglo del estado para poder imprimirlo en pantalla mediante parrafos usando JSX.

## <a name="lesson09"></a> Lección #9 - Peticiones AJAX
- - -
Para esta lección, usaremos la página [jsonplaceholder](https://jsonplaceholder.typicode.com/) para hacer pruebas con peticiones AJAX.

Una buena práctica de programación es no delegarle muchas tareas al constructor, es decir, no debe estar cargado. El motivo es por el testing, pruebas automatizadas, etc.

En el caso de los componentes de React hay algo que se llama los [métodos del ciclo de vida](https://es.reactjs.org/docs/state-and-lifecycle.html). Se tratan de métodos especiales que podemos definir en un componente que se van a ejecutar en puntos clave. En componentDidMount sería un buen lugar para realizar nuestras peticiones AJAX sin cargar nuestro constructor y sería al principio del ciclo de vida de nuestro componente.

Para las peticiones AJAX se usará la librería fetch.

## <a name="lesson10"></a> Lección #10 - Estilos
- - -
Uso de estilos inline y hojas de estilos en JSX.

## <a name="lesson11"></a> Lección #11 - React Hooks
- - -
En esta lección, manejaremos los estados y los ciclos de vida mediante componentes funcionales. Esta era una habilidad que solo poseían las clases en React pero ahora es posible para componentes funcionales.

Existen dos tipos de Hooks:
1. Los hooks de la librería de React (los predefinidos).
2. Los hooks personalizados de otras librerías.

En esta lección, realizaremos el contador de lecciones anteriores.

*Nota:* A pesar de que los hooks son muy nuevos, traen sus propias limitantes... Una de ellas es que no podemos usar la función useState() dentro de condicionales por ejemplo. El resto de Hooks serán vistos en el curso profesional de React.

## <a name="lesson12"></a> Lección #12 - Separar componentes en archivos
- - -
